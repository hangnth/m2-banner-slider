<?php

namespace Magenest\Slider\Model\Config\Source;

class DatePicker extends \Magento\Backend\Block\Template
{
    protected $elementFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Data\Form\Element\Factory $elementFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->elementFactory = $elementFactory;
    }

    public function prepareElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $input = $this->elementFactory->create("text", ['data' => $element->getData()]);
        $input->setId($element->getId());
        $input->setForm($element->getForm());
        $input->setClass("admin__control-text input-text no-changes");
        if ($element->getRequired()) {
            $input->addClass('required-entry');
        }

        $id      = '#' . $element->getHtmlId();
        $addHtml = '';
        if ($this->getRequest()->isXmlHttpRequest()) {
            $addHtml .= '<script type="text/javascript">
                require(["jquery", "jquery-ui-modules/datepicker"], function () {
                    $(document).ready(function () {
                        $("#' . $element->getHtmlId() . '").calendar({
                            dateFormat:"yy-mm-dd",
                            showButtonPanel: true,
                            changeMonth: true,
                            changeYear: true,
                            yearRange: "-c-10:c+05"
                        });
                        $(".ui-datepicker-trigger").removeAttr("style");
                        $(".control-value").hide();
                        $(".ui-datepicker-trigger").click(function(){
                            $(this).prev().focus();
                        });
                    });
                });
            </script>';
        } else {
            $addHtml .= '<script type="text/x-magento-init">
                {"*": {"Magenest_Slider/js/date-picker":{"id":"' . $id . '"}}}
                </script>';
        }
        return $element->setData('after_element_html', $input->getElementHtml() . $addHtml);
    }
}
