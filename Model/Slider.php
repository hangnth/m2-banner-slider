<?php

namespace Magenest\Slider\Model;

class Slider extends \Magento\Framework\Model\AbstractModel
{
    protected $_eventPrefix = 'magenest_slider';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Slider\Model\ResourceModel\Slider $resource,
        \Magenest\Slider\Model\ResourceModel\Slider\Collection $resourceCollection
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection);
        $this->_init(ResourceModel\Slider::class);
    }

    public function isActive()
    {
        return (bool)$this->getStatus();
    }
}
