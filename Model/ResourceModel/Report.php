<?php

namespace Magenest\Slider\Model\ResourceModel;

class Report extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * construct
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('magenest_slider_report', 'report_id');
    }
}
