<?php

namespace Magenest\Slider\Model\ResourceModel\Report;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'report_id';

    protected function _construct()
    {
        $this->_init('Magenest\Slider\Model\Report', 'Magenest\Slider\Model\ResourceModel\Report');
    }
}
