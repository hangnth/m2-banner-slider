<?php

namespace Magenest\Slider\Model;

class Report extends \Magento\Framework\Model\AbstractModel
{
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Slider\Model\ResourceModel\Report $resource,
        \Magenest\Slider\Model\ResourceModel\Report\Collection $resourceCollection
    ) {
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection
        );
    }
}
