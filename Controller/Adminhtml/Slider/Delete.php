<?php

namespace Magenest\Slider\Controller\Adminhtml\Slider;

use Magento\Backend\App\Action;
use Magento\Framework\App\Cache\TypeListInterface as CacheTypeListInterface;
use Magento\Framework\App\ResponseInterface;

class Delete extends \Magenest\Slider\Controller\Adminhtml\Slider
{

    protected $sliderFactory;

    /**
     * @var \Magenest\Slider\Model\ResourceModel\Slider
     */
    private $sliderResourceModel;

    protected $cache;

    /**
     * Delete constructor.
     *
     * @param \Magento\Backend\App\Action\Context            $context
     * @param \Magenest\Slider\Model\SliderFactory           $sliderFactory
     * @param \Magenest\Slider\Model\ResourceModel\Slider    $sliderResourceModel
     * @param \Magento\Framework\App\Cache\TypeListInterface $cache
     */
    public function __construct(
        Action\Context $context,
        \Magenest\Slider\Model\SliderFactory $sliderFactory,
        \Magenest\Slider\Model\ResourceModel\Slider $sliderResourceModel,
        CacheTypeListInterface $cache
    ) {
        $this->sliderFactory = $sliderFactory;
        $this->sliderResourceModel = $sliderResourceModel;
        $this->cache = $cache;

        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $model = $this->sliderFactory->create();
                $this->sliderResourceModel->load($model, $id);
                $this->sliderResourceModel->delete($model);

                // Display invalidate cache
                $this->cache->invalidate(['layout', 'block_html', 'full_page']);
                $this->messageManager->addSuccessMessage(__('You deleted the Slider.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a Slider to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
