<?php

namespace Magenest\Slider\Controller\Adminhtml\Slider;

use Magenest\Slider\Controller\Adminhtml\Slider;

/**
 * Class NewAction
 *
 * @package Magenest\Slider\Controller\Adminhtml\Slider
 */
class NewAction extends Slider
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $this->_forward('edit');
    }
}
