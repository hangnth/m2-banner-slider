<?php

namespace Magenest\Slider\Controller\Adminhtml\Slider;

use Magento\Backend\App\Action;
use Magento\Framework\App\Cache\Manager as CacheManager;
use Magento\Framework\App\Cache\TypeListInterface as CacheTypeListInterface;
use Magento\Framework\App\ResponseInterface;

class Duplicate extends \Magenest\Slider\Controller\Adminhtml\Slider
{
    protected $sliderFactory;

    /**
     * @var \Magenest\Slider\Model\ResourceModel\Slider
     */
    private $sliderResourceModel;
    /**
     * @var \Magenest\Slider\Model\ResourceModel\Slider
     */
    private $itemResourceModel;
    /**
     * @var \Magenest\Slider\Model\ItemFactory
     */
    protected $itemFactory;
    /**
     * @var CacheTypeListInterface
     */

    protected $cache;
    /**
     * @var CacheManager
     */
    protected $cacheManager;

    /**
     * Duplicate constructor.
     *
     * @param \Magento\Backend\App\Action\Context            $context
     * @param \Magenest\Slider\Model\SliderFactory           $sliderFactory
     * @param \Magenest\Slider\Model\ItemFactory             $itemFactory
     * @param \Magenest\Slider\Model\ResourceModel\Slider    $sliderResourceModel
     * @param \Magenest\Slider\Model\ResourceModel\Item      $itemResourceModel
     * @param \Magento\Framework\App\Cache\TypeListInterface $cache
     * @param \Magento\Framework\App\Cache\Manager           $cacheManager
     */
    public function __construct(
        Action\Context $context,
        \Magenest\Slider\Model\SliderFactory $sliderFactory,
        \Magenest\Slider\Model\ItemFactory $itemFactory,
        \Magenest\Slider\Model\ResourceModel\Slider $sliderResourceModel,
        \Magenest\Slider\Model\ResourceModel\Item $itemResourceModel,
        CacheTypeListInterface $cache,
        CacheManager $cacheManager
    ) {
        $this->sliderFactory = $sliderFactory;
        $this->itemFactory = $itemFactory;
        $this->sliderResourceModel = $sliderResourceModel;
        $this->itemResourceModel = $itemResourceModel;
        $this->cache = $cache;
        $this->cacheManager = $cacheManager;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $sliderModel = $this->sliderFactory->create();
                $itemModel = $this->itemFactory->create();

                //save slider
                $this->sliderResourceModel->load($sliderModel, $id);
                $sliderModel->unsetData('slider_id');
                $sliderModel->setData('status', '0');
                $this->sliderResourceModel->save($sliderModel);
                $parentSlider = $sliderModel->getId();

                //save item
                $items = $itemModel->getCollection()->addFieldToFilter('slider_id', ['eq' => $id]);
                foreach ($items as $item){
                    $this->itemResourceModel->load($itemModel, $item->getItemId());
                    $itemModel->unsetData('item_id');
                    $itemModel->addData(
                        [
                            'slider_id' => $sliderModel->getId(),
                            'data_source' => $itemModel->getData('data_source'),
                            'order_number' => $itemModel->getData('order_number')
                        ]
                    );
                    $this->itemResourceModel->save($itemModel);
                }
                if($sliderModel->getData('type') == 2) {
                    $childSlider = $sliderModel->getCollection()->addFieldToFilter('parent_id', ['eq' => $id])->getFirstItem();
                    $this->sliderResourceModel->load($sliderModel, $childSlider->getId());
                    $sliderModel->setData('status', '0');
                    $sliderModel->setData('parent_id',  $parentSlider);
                    $sliderModel->unsetData('slider_id');
                    $this->sliderResourceModel->save($sliderModel);

                    //save item
                    $items = $itemModel->getCollection()->addFieldToFilter('slider_id', ['eq' => $childSlider->getId()]);
                    foreach ($items as $item){
                        $this->itemResourceModel->load($itemModel, $item->getItemId());
                        $itemModel->unsetData('item_id');
                        $itemModel->addData(
                            [
                                'slider_id' => $sliderModel->getId(),
                                'data_source' => $itemModel->getData('data_source'),
                                'order_number' => $itemModel->getData('order_number')
                            ]
                        );
                        $this->itemResourceModel->save($itemModel);
                    }
                }
                // Display invalidate cache
                $this->cache->invalidate(['layout', 'block_html', 'full_page']);
                $this->messageManager->addSuccessMessage(__('Slider data has been successfully duplicated.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a Slider to duplicate.'));
        return $resultRedirect->setPath('*/*/');
    }
}
