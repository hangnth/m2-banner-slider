<?php

namespace Magenest\Slider\Controller\Adminhtml\Slider;

use Magento\Backend\App\Action;

class GetCategoryInfo extends Action
{
    const ADMIN_RESOURCE = 'Magenest_Slider::manage';

    protected $categoryFactory;

    protected $resourceCategory;

    public function __construct(
        Action\Context $context,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\ResourceModel\Category $resourceCategory
    ) {
        $this->categoryFactory  = $categoryFactory;
        $this->resourceCategory = $resourceCategory;
        parent::__construct($context);
    }

    public function execute()
    {
        $categoryModel = $this->categoryFactory->create();
        $categoryId    = $this->getRequest()->getParam('categoryId');

        $this->resourceCategory->load($categoryModel, $categoryId);
        // TODO: Implement execute() method.
        $response = $this->resultFactory
            ->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
            ->setData(
                [
                'status'   => true,
                'url'      => $categoryModel->getUrl(),
                'imageUrl' => $categoryModel->getData('image') ? $categoryModel->getData('image') : '',
                'title'    => $categoryModel->getName(),
                'desc'     => $categoryModel->getData('description'),
                ]
            );
        return $response;
    }
}
