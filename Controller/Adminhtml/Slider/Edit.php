<?php

namespace Magenest\Slider\Controller\Adminhtml\Slider;

use Magenest\Slider\Model\ResourceModel\Slider;
use Magento\Backend\App\Action;
use Magento\Framework\Registry;

class Edit extends \Magenest\Slider\Controller\Adminhtml\Slider
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    protected $sliderFactory;

    protected $resourceModel;

    /**
     * Edit constructor.
     *
     * @param \Magento\Backend\App\Action\Context         $context
     * @param \Magento\Framework\Registry                 $coreRegistry
     * @param \Magenest\Slider\Model\SliderFactory        $sliderFactory
     * @param \Magenest\Slider\Model\ResourceModel\Slider $resourceModel
     */
    public function __construct(
        Action\Context $context,
        Registry $coreRegistry,
        \Magenest\Slider\Model\SliderFactory $sliderFactory,
        Slider $resourceModel
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->sliderFactory = $sliderFactory;
        $this->resourceModel = $resourceModel;
        parent::__construct($context);
    }

    /**
     * Edit site map
     *
     * @return                                  void
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id    = $this->getRequest()->getParam('id');
        $model = $this->sliderFactory->create();

        // 2. Initial checking
        if ($id) {
            $this->resourceModel->load($model, $id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This item no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }

        // 3. Set entered data if was error when we do save
        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        // 4. Register model to use later in blocks
        $this->_coreRegistry->register('magenest_slider_slider', $model);

        // 5. Build edit form
        $this->_initAction()->_addBreadcrumb(
            $id ? __('Edit %1', $model->getName()) : __('New Slider'),
            $id ? __('Edit %1', $model->getName()) : __('New Slider')
        );
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Edit Slider'));
        $this->_view->getPage()->getConfig()->getTitle()->prepend(
            $model->getId() ? $model->getName() : __('New Slider')
        );
        $this->_view->renderLayout();
    }
}
