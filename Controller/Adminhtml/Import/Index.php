<?php

namespace Magenest\Slider\Controller\Adminhtml\Import;

use Magento\Backend\App\Action;

class Index extends Action
{
    const ADMIN_RESOURCE = 'Magenest_Slider::import';

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultFactory->create('page');
        $resultPage->setActiveMenu('Magenest_Slider::manage');
        return $resultPage;
    }
}
