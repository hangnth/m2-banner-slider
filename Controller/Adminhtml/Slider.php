<?php

namespace Magenest\Slider\Controller\Adminhtml;

use Magento\Backend\App\Action;

abstract class Slider extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Magenest_Slider::manage';

    /**
     * Init actions
     *
     * @return $this
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        $this->_view->loadLayout();
        $this->_setActiveMenu(
            'Magenest_Slider::manage'
        )->_addBreadcrumb(
            __('Buttons'),
            __('Buttons')
        );
        return $this;
    }
}
