<?php

namespace Magenest\Slider\Controller\Index;

class Click extends \Magenest\Slider\Controller\Index
{
    public function execute()
    {
        $resultRaw = $this->_resultRawFactory->create();
        $userCode = $this->getUserCode(null);
        $date = $this->_stdTimezone->date()->format('Y-m-d');
        $sliderId = $this->getRequest()->getParam('slider_id');
        $slider = $this->_sliderFactory->create()->load($sliderId);
        $name = $slider->getData('name');

        if ($slider->getId()) {
//            if ($this->getCookieManager()->getCookie('slider_user_code_click'.$sliderId) === null) {
//                $this->getCookieManager()->setPublicCookie('slider_user_code_click'.$sliderId, $userCode);
                $reportCollection = $this->_reportCollectionFactory->create()
                    ->addFieldToFilter('date_click', $date)
                    ->addFieldToFilter('name', $name)
                    ->addFieldToFilter('slider_id', $sliderId)
                    ->setPageSize(1)->setCurPage(1);

                if ($reportCollection->getSize()) {
                    $reportFirstItem = $reportCollection->getFirstItem();
                    $reportFirstItem->setClicks($reportFirstItem->getClicks() + 1);
                    try {
                        $reportFirstItem->save();

                    } catch (\Exception $e) {
                        $this->_monolog->addError($e->getMessage());
                    }
                } else {
                    $report = $this->_reportFactory->create()
                        ->setSliderId($slider->getId())
                        ->setClicks(1)
                        ->setData('date_click', $date)
                        ->setData('name', $name);
                    try {
                        $report->save();

                    } catch (\Exception $e) {
                        $this->_monolog->addError($e->getMessage());
                    }
                }
//            }
        }

        return $resultRaw;
    }
}
