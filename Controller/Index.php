<?php

namespace Magenest\Slider\Controller;

abstract class Index extends \Magento\Framework\App\Action\Action
{

    protected $_sliderFactory;

    protected $_reportFactory;

    protected $_reportCollectionFactory;

    protected $_resultRawFactory;

    protected $_monolog;

    protected $_stdTimezone;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magenest\Slider\Model\SliderFactory $sliderFactory,
        \Magenest\Slider\Model\ReportFactory $reportFactory,
        \Magenest\Slider\Model\ResourceModel\Report\CollectionFactory $reportCollectionFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\Logger\Monolog $monolog,
        \Magento\Framework\Stdlib\DateTime\Timezone $stdTimezone
    ) {
        parent::__construct($context);
        $this->_sliderFactory           = $sliderFactory;
        $this->_reportFactory           = $reportFactory;
        $this->_reportCollectionFactory = $reportCollectionFactory;
        $this->_resultRawFactory        = $resultRawFactory;
        $this->_monolog                 = $monolog;
        $this->_stdTimezone             = $stdTimezone;
    }


    public function getCookieManager()
    {
        return $this->_objectManager->create('Magento\Framework\Stdlib\CookieManagerInterface');
    }

    /**
     * get user code.
     *
     * @param mixed $id
     *
     * @return string
     */
    protected function getUserCode($id)
    {
        $ipAddress      = $this->_objectManager->create('Magento\Framework\HTTP\PhpEnvironment\Request')->getClientIp(true);
        $cookieFrontend = $this->getCookieManager()->getCookie('frontend');
        $userCode       = $ipAddress . $cookieFrontend . $id;

        return hash('md5', $userCode);
    }
}
