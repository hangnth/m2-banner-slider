'use strict';
define(
    [
    'jquery',
    'jquery/ui'
    ], function ($) {

        $.widget(
            'magenest.clickslider', {
                options: {
                    url: '',
                    slider_id: '',
                },

                _create: function () {
                    var o = this.options;
                    this.element.each(
                        function (index, el) {
                            $(el).click(
                                function (event) {
                                    $.ajax(
                                        {
                                            url: o.url,
                                            type: 'POST',
                                            dataType: 'html',
                                            data: {
                                                slider_id: o.slider_id
                                            },
                                        }
                                    ).done(
                                        function () {
                                            console.log("success");
                                        }
                                    );

                                }
                            );
                        }
                    );

                },
            }
        );
        return $.magenest.clickslider;
    }
);
