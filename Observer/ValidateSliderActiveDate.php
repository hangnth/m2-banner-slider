<?php
/**
 * ValidateSliderActiveDate
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\Slider\Observer;

use Magenest\Slider\Block\Widget\SliderView;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Widget\Model\Widget\Instance;

class ValidateSliderActiveDate implements ObserverInterface
{
    protected $datetime;

    protected $serializer;

    public function __construct(
        DateTime $datetime,
        SerializerInterface $serializer
    ) {
        $this->datetime   = $datetime;
        $this->serializer = $serializer;
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        /**
         * @var \Magento\Widget\Model\Widget\Instance $instance
         */
        $instance = $observer->getObject();
        if ($instance instanceof Instance && $instance->getType() == SliderView::class) {
            $parameters = $instance->getWidgetParameters();
            if (isset($parameters['slider_id'], $parameters['active_from'], $parameters['active_to'])) {
                $current        = $this->datetime->date('Y-m-d');
                $activeFrom     = $parameters['active_from'];
                $activeDateFrom = $this->datetime->date('Y-m-d', $activeFrom);
                $activeTo       = $parameters['active_to'];
                $activeDateTo   = $this->datetime->date('Y-m-d', $activeTo);
                if ($activeFrom && $activeDateFrom < $current) {
                    throw new \InvalidArgumentException(__('Active from date must be greater than or equal current date'));
                }
                if ($activeTo && $activeDateTo < $current) {
                    throw new \InvalidArgumentException(__('Active to date must be greater than or equal current date'));
                }
                if ($activeFrom && $activeTo && $activeDateFrom > $activeDateTo) {
                    throw new \InvalidArgumentException(__('Active to date must be greater than or equal active from date'));
                }
                if ($activeFrom) {
                    $parameters['active_from'] = $activeDateFrom;
                }
                if ($activeTo) {
                    $parameters['active_to'] = $activeDateTo;
                }
                $instance->setWidgetParameters($this->serializer->serialize($parameters));
            }
        }
    }
}
