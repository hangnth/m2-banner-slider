<?php
/**
 * ActiveDateSlider
 *
 * @copyright Copyright © 2020 Magenest JSC. All rights reserved.
 * @author    dangnh@magenest.com
 */

namespace Magenest\Slider\Cron;

use Magenest\Slider\Block\Widget\SliderView;
use Magenest\Slider\Model\ResourceModel\Slider;
use Magenest\Slider\Model\SliderFactory;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Stdlib\DateTime\DateTime;

class ActiveDateSlider
{
    protected $widgetCollectionFactory;

    protected $resourceSlider;

    protected $sliderFactory;

    protected $serializer;

    protected $datetime;

    protected $cacheTypeList;

    public function __construct(
        \Magento\Widget\Model\ResourceModel\Widget\Instance\CollectionFactory $widgetCollectionFactory,
        Slider $resourceSlider,
        SliderFactory $sliderFactory,
        Json $serializer,
        DateTime $datetime,
        TypeListInterface $cacheTypeList
    ) {
        $this->widgetCollectionFactory = $widgetCollectionFactory;
        $this->resourceSlider          = $resourceSlider;
        $this->sliderFactory           = $sliderFactory;
        $this->serializer              = $serializer;
        $this->datetime                = $datetime;
        $this->cacheTypeList           = $cacheTypeList;
    }

    public function execute()
    {
        /**
         * @var \Magento\Widget\Model\ResourceModel\Widget\Instance\Collection $widgetCollection
         */
        $widgetCollection = $this->widgetCollectionFactory->create();
        $widgetCollection->addFieldToFilter('instance_type', SliderView::class);
        $refresh = false;
        /**
         * @var \Magento\Widget\Model\Widget\Instance $widget
         */
        foreach ($widgetCollection as $widget) {
            $parameters = $widget->getWidgetParameters();
            if (isset($parameters['slider_id'], $parameters['active_from'], $parameters['active_to'])) {
                $sliderId   = $parameters['slider_id'];
                $activeFrom = $parameters['active_from'];
                $activeTo   = $parameters['active_to'];
                $slider     = $this->sliderFactory->create();
                $this->resourceSlider->load($slider, $sliderId);
                if ($slider->getId() && $slider->isActive()) {
                    $current = $this->datetime->timestamp();
                    if ($this->isActiveFromValid($activeFrom, $current) || $this->isActiveToValid($activeTo, $current)) {
                        $refresh = true;
                        break;
                    }
                }
            }
        }
        if ($refresh) {
            $this->cleanCache();
        }
    }

    public function isActiveFromValid($activeFrom, $current)
    {
        return $activeFrom && (abs(($this->datetime->timestamp($activeFrom) - $current) / (60 * 60 * 24)) <= 1); //TODO: only runs once
    }

    public function isActiveToValid($activeTo, $current)
    {
        return $activeTo && (abs(($current - $this->datetime->timestamp($activeTo)) / (60 * 60 * 24) <= 1)); //TODO: only runs once
    }

    public function cleanCache($types = ['full_page', 'block_html', 'layout'])
    {
        foreach ($types as $type) {
            $this->cacheTypeList->cleanType($type);
        }
    }
}
