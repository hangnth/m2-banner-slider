# Change Log
All notable changes to this extension will be documented in this file.
This extension adheres to [Magenest](https://magenest.com/).

## [1.1.0] - 10/04/2020
- Compatible with Magento 2.3
- Import slider from CSV, include sample data
- Report click page from storefront
- Add banner static link
- Add active date
- Duplicate banner

## [1.0.0] - 20/05/2019
### Admin
- Create Banner, Slider, Sync Slider
- Set position show
