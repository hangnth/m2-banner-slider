<?php

namespace Magenest\Slider\Block\Adminhtml\Import;

use \Magento\Backend\Block\Widget\Form\Container;

/**
 * Class Edit
 *
 * @package Magenest\Slider\Block\Adminhtml\Import
 */
class Edit extends Container
{

    public function _construct()
    {
        parent::_construct();
        $this->buttonList->remove('reset');
        $this->buttonList->update('save', 'label', __('Upload'));
        $this->buttonList->update('save', 'id', 'upload_button');
        $this->_objectId = 'id';
        $this->_blockGroup = 'Magenest_Slider';
        $this->_controller = 'adminhtml_import';

        $this->addButton(
            'back',
            [
                'label' => __('Back'),
                'onclick' => 'setLocation(\'' . $this->getUrl('slider/slider/index') . '\')',
                'class' => 'back'
            ],
            -1
        );

    }
}
